# CodeceptJs Playwright

This repo is just testing proof of concept if CodeceptJS with Puppeteer can be used for multi session e2e testing.

### Installation
```
git clone https://gitlab.com/toffer.lim87/codeceptjs-puppeteer.git
cd codeceptjs-puppeteer
npm install

```

### Scripts Commands
To run the test:
```
npm run test:e2e
```

To create a test file:
```
npm run create:test
```

To create a page object file:
```
npm run create:page
```

### Resources
CodeceptJS Getting Started - [https://codecept.io/basics/](https://codecept.io/basics/) <br />
Testing with Playwright - [https://codecept.io/playwright/](https://codecept.io/playwright/)
