const { devices } = require('playwright');

Feature('Mobile Testing');

Scenario('website looks nice on iPhone', ({ I }) => {
  session('iPhone 6', devices['iPhone 6'], () => {
    I.amOnPage('/');
    I.see('todos');
    I.waitForClickable('input');
  });

  session('iPhone X', devices['iPhone X'], () => {
    I.amOnPage('/');
    I.see('todos');
    I.waitForClickable('input');
  });
});

Scenario('website looks nice on Android Phone', ({ I }) => {
  session('Pixel 2', devices['Pixel 2'], () => {
    I.amOnPage('/');
    I.see('todos');
    I.waitForClickable('input');
  });

  session('Galaxy S5', devices['Galaxy S5'], () => {
    I.amOnPage('/');
    I.see('todos');
    I.waitForClickable('input');
  });
});
