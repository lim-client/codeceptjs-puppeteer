const { expect } = require("chai");

const { I } = inject();
// declaring page object as a class
class ClassPage {
  constructor() {
    this.header = '.learn h3';
    this.subheader = '.source-links h5';
  };

  async getHeader(){
    I.waitForElement(this.header);
    const header = await I.grabTextFrom(this.header);
    console.log('This is a Header: ' + header)
    return header;
  };

  async getSubHeader(index){
    // await I.waitForInvisible(locate(this.subheader).at(index));
    const subheader = await I.grabTextFrom(locate(this.subheader).at(index));
    console.log('This is a Subheader: ' + subheader);
    return subheader;
  };

};

// For inheritance
module.exports = new ClassPage();
module.exports.ClassPage = ClassPage;